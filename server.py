from io import BytesIO
import numpy as np
import threading
import time
import cv2
import json
import tornado.ioloop
import tornado.web


image_mutex = threading.Semaphore()
images = []
stop_camera = threading.Event()
pipeline = None


class Type:
    def __init__(self, name):
        self.name = name


class Input:
    def __init__(self, name, type):
        self.name = name
        self.type = type
    
    def to_dict(self):
        return {
            'name': self.name,
            'type': self.type,
        }


class Block:
    def __init__(self, name, run, inputs=None, output=None):
        if inputs is None and output is None:
            raise Exception('`inputs` or `output` must be set')
        self.name = name
        self.run = run
        self.inputs = inputs
        self.output = output
    def to_dict(self):
        obj = {'name': self.name}
        if self.inputs is not None:
            obj['inputs'] = [input.to_dict() for input in self.inputs]
        if self.output is not None:
            obj['output'] = self.output
        return obj


class PipelineContext:
    def __init__(self, cap, images):
        self.cap = cap
        self.images = images


def camera(context, inputs):
    ok, image = context.cap.read()
    if not ok:
        raise Exception('cannot capture image')
    return image


def crop(context, inputs):
    img = inputs['img']
    (maxx, maxy, _) = img.shape
    x = inputs['x']
    y = inputs['y']
    x2 = min(maxx, x + inputs['dx'])
    y2 = min(maxy, y + inputs['dy'])
    return img[x:x2, y:y2]


def binarize(context, inputs):
    img = inputs['img']
    newImg = np.copy(img)
    r = inputs['r']
    g = inputs['g']
    b = inputs['b']
    k = inputs['k']
    mask = img[:, :, 0]*r + img[:, :, 1]*g + img[:, :, 2]*b > k
    newImg[mask, 0:2] = 255
    newImg[~mask, 0:2] = 0
    return newImg 


def stream(context, inputs):
    context.images.append(inputs['img'])


BLOCKS = [
    Block("camera", output='img', run=camera),
    Block("crop", 
        inputs=[
            Input(name='img', type='img'),
            Input(name='x', type='int'),
            Input(name='y', type='int'),
            Input(name='dx', type='int'),
            Input(name='dy', type='int'),
        ],
        output='img',
        run=crop,
    ),
    Block("binarize",
        inputs=[
            Input(name='img', type='img'),
            Input(name='r', type='int'),
            Input(name='g', type='int'),
            Input(name='b', type='int'),
            Input(name='k', type='int'),
        ],
        output='img',
        run=binarize,
    ),
    Block("stream",
        inputs=[
            Input(name='img', type='img'),
        ],
        run=stream,
    ),
]

BLOCK_BY_NAME = {block.name: block for block in BLOCKS}


class PipelineHandler(tornado.web.RequestHandler):
    def get(self):
        global pipeline
        self.add_header('content-type', 'application/json')
        self.write(json.dumps(pipeline))
    def post(self):
        global pipeline
        if self.request.headers.get('content-type') != 'application/json':
            self.set_status(400)
            return
        try:
            pipeline = json.loads(self.request.body)
            self.set_status(200)
            self.write('pipeline updated')
        except json.JSONDecodeError:
            self.set_status(500)
            self.write('bad JSON content')
        

class BlocksHandler(tornado.web.RequestHandler):
    def get(self):
        obj = { block.name: block.to_dict() for block in BLOCKS }
        self.set_header('Content-Type', 'application/json')
        self.write(json.dumps(obj)) 


class LiveHandler(tornado.web.RequestHandler):
    def get(self, index):
        global images
        if not index:
            self.add_header('Content-Type', 'application/json')
            self.write(json.dumps(len(images)))
        else:
            index = int(index)
            try:
                with image_mutex:
                    image = images[index]
                _, png = cv2.imencode(".png", image)
                self.set_header("Content-Type", "image/png")
                self.write(png.tobytes())
            except IndexError:
                self.set_status(400)
                self.write("Cannot get image at index %d" % index)
            except:
                self.set_status(500)
                self.write("Cannot get image")


def execute_pipeline(context):
    global pipeline, images
    if pipeline is None:
        return
    blocks = sorted(pipeline.items(), key=lambda e: e[0], reverse=True)
    scope = {}
    while blocks:
        id0, blockObj = blocks.pop()
        block = BLOCK_BY_NAME[blockObj['block']]
        inputs = {}
        if block.inputs:
            inputsObj = blockObj['inputs']
            for declInput, actualInput in zip(block.inputs, inputsObj):
                name = declInput.name
                value = actualInput['value']
                if declInput.type == 'int':
                    inputs[name] = value
                else:
                    inputs[name] = scope[value]
        result = block.run(context, inputs)
        if block.output:
            scope[id0] = result
        

def camera_thread():
    global images
    cap = cv2.VideoCapture(0)
    try:
        while not stop_camera.is_set():
            context = PipelineContext(cap, [])
            execute_pipeline(context)
            with image_mutex:
                images = context.images
            time.sleep(1.0/30)
    finally:
        cap.release()


def main():
    thread = threading.Thread(target=camera_thread)
    thread.start()
    app = tornado.web.Application([
        (r"/pipeline", PipelineHandler),
        (r"/blocks", BlocksHandler),
        (r"/live(?:/(?P<index>\d+)/.*)?", LiveHandler),
        (r"/(.*)", tornado.web.StaticFileHandler, {
            "path": "./static",
            "default_filename": "index.html",
        }),
    ])
    app.listen(8888)
    try:
        tornado.ioloop.IOLoop.current().start()
    except KeyboardInterrupt:
        pass
    finally:
        stop_camera.set()
        thread.join()


if __name__ == "__main__":
    main()