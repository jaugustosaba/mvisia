Requirements:

- Python 3.6.9
- OpenCV 3.2.0
- Numpy 1.13.3
- Tornado 4.5.3
- Typescript 2.7.2

Running:

  $ python3 server.py

Address:

  http://localhost:8888

