type Input = {
    name: string,
    type: string,
}

class Block {
    constructor(
        readonly name: string, 
        readonly inputs: Input[], 
        readonly output: string) {
    }
}

class BlockList {
    private blocks: Block[];
    private list: HTMLUListElement;
    private blockTempl: HTMLLIElement;
    public pipeline: Pipeline = null;

    constructor(readonly root: HTMLElement) {
        this.blocks = [];
        this.list = root.querySelector('ul.blocks');
        this.blockTempl = this.list.querySelector('div.template li.block');
    }

    public getBlock(name: string): Block {
        for (let block of this.blocks) {
            if (block.name == name)
                return block;
        }
        return null;
    }

    private reset() {
        let allLIs = this.root.querySelectorAll('div.blocks ul.blocks > li');
        for (let i=0; i < allLIs.length; ++i) {
            let LI = allLIs.item(i);
            LI.remove();
        }
    }

    public load(done?: ()=>void) {
        this.reset();
        const req = new XMLHttpRequest();
        req.open('get', '/blocks');
        req.onload = () => {
            let blocks = JSON.parse(req.responseText);
            for (let name in blocks) {
                let block = blocks[name];
                this.addBlock(name, block['inputs'], block['output'])
            }
            if (done)
                done();
        };
        req.onerror = () => {
            alert('Cannot get list of blocks');
        };
        req.send();
    }

    private addBlock(name: string, inputs: any, output: string) {
        let block = new Block(name, inputs, output);
        let blockEl = this.blockTempl.cloneNode(true) as HTMLLIElement;
        let nameEl = blockEl.querySelector('p.name');
        nameEl.innerHTML = name;
        let instantiateEl = blockEl.querySelector('input.instantiate') as HTMLButtonElement;
        instantiateEl.addEventListener('click', (ev: MouseEvent) => {
            this.pipeline.instantiate(block);
        });
        this.list.appendChild(blockEl);
        this.blocks.push(block);
    }
}

abstract class Editor {
    constructor(readonly root: HTMLElement, readonly name: string, readonly type: string) {
    }

    abstract hasValue(): boolean;
    abstract value(): number;
    abstract setValue(value: any): void;
}

class NumberEditor extends Editor {
    constructor(root: HTMLInputElement, readonly name: string) {
        super(root, name, 'int');
    }

    hasValue(): boolean {
        let input = this.root as HTMLInputElement;
        return (input.value != '') && input.willValidate;
    }

    value(): number {
        if (!this.hasValue()) return null;
        return (this.root as HTMLInputElement).valueAsNumber;
    }

    setValue(value: any) {
        (this.root as HTMLInputElement).valueAsNumber = value;
    }
}

class OpaqueEditor extends Editor {
    constructor(root: HTMLSelectElement, name:string, type: string) {
        super(root, name, type);
    }

    hasValue(): boolean {
        return (this.root as HTMLSelectElement).selectedIndex > 0;
    }

    value(): number {
        if (!this.hasValue()) return null;
        let el = this.root as HTMLSelectElement;
        return parseInt(el.selectedOptions.item(0).value);
    }

    setValue(value: any) {
        let el = this.root as HTMLSelectElement;
        el.selectedIndex = 0;
        for (let i=1; i < el.options.length; ++i) {
            let option = el.options.item(i);
            if (option.value == value) {
                el.selectedIndex = i;
                break;
            }
        }
    }

    updateOptions(options: number[]) {
        let root = this.root as HTMLSelectElement;

        // saves old selection
        let curSel: string;
        if (root.selectedIndex >= 0) {
            curSel = root.options.item(root.selectedIndex).value;
        }

        while (root.hasChildNodes()) {
            root.removeChild(root.firstChild);
        }
        root.appendChild(document.createElement('option'));
        for (let option of options) {
            let optionEl = document.createElement('option') as HTMLOptionElement;
            optionEl.value = `${option}`;
            optionEl.innerText = optionEl.value;
            optionEl.selected = (curSel == optionEl.value);
            root.appendChild(optionEl);
        }
    }
}

class BlockInstance {
    constructor(
        readonly id: number,
        readonly block: Block, 
        readonly editors: Editor[],
        readonly root: HTMLDivElement) {
    }

    isValid(): boolean {
        for (let editor of this.editors)
            if (!editor.hasValue())
                return false;
        return true;
    }
}

class Live {
    private liveTmpl: HTMLDivElement;
    private timeoutIds: number[] = [];

    constructor(readonly root: HTMLDivElement) {
        this.liveTmpl = root.querySelector('div.template div.live');
    }

    private clearTimeouts() {
        for (let timeoutId of this.timeoutIds)
            clearInterval(timeoutId);
        this.timeoutIds = [];
    }

    private reset() {
        this.clearTimeouts();
        let allDivs = this.root.querySelectorAll(':scope > div.live');
        for (let i=0; i < allDivs.length; ++i)
            allDivs.item(i).remove();
    }

    private fetchLiveCount(cb: (liveCount: number) => void) {
        let req = new XMLHttpRequest();
        req.open('get', '/live');
        req.onload = () => {
            let lives = parseInt(req.responseText.trim());
            cb(lives);
        }
        req.send();
    }

    private appendImage(index: number) {
        let divEl = this.liveTmpl.cloneNode(true) as HTMLDivElement;
        this.root.appendChild(divEl);
        divEl.querySelector('h2').innerText = `Stream ${index}`;
        let imageEl = divEl.querySelector('img');
        let dummy = 0;
        this.timeoutIds.push(setInterval(() => {
            imageEl.src = `/live/${index}/${dummy++}`;
        }, 250));
    }

    public update() {
        this.reset();
        this.fetchLiveCount((liveCount) => {
            for (let i=0; i < liveCount; ++i) {
                this.appendImage(i);
            }
        });
    }
}

const UPDATE_INSTANCE: string = 'update-instance';
const UPDATE_OPAQUES: string = 'update-opaques';


class Pipeline {
    private instances: BlockInstance[];
    private list: HTMLDivElement;
    private instanceTmpl: HTMLDivElement;
    private numberEditorTmpl: HTMLDivElement;
    private opaqueInputTmpl: HTMLInputElement;
    private inputTmpl: HTMLSelectElement;
    private saveBtn: HTMLButtonElement;
    private resetBtn: HTMLButtonElement;
    private lastId: number = 0;
    public blockList: BlockList;
    public live: Live;

    constructor(readonly root: HTMLElement) {
        this.instances = [];
        this.list = root.querySelector('div.instances')
        this.instanceTmpl = this.list.querySelector('div.template div.instance');
        this.numberEditorTmpl = this.list.querySelector('div.template div.number-editor input');
        this.opaqueInputTmpl = this.list.querySelector('div.template div.opaque-editor select');
        this.inputTmpl = this.list.querySelector('div.template div.input tr');
        this.setupCtrls();
    }

    private setupCtrls() {
        this.saveBtn = this.root.querySelector('div.control input.save') as HTMLButtonElement;
        this.saveBtn.addEventListener('click', (ev: MouseEvent) => this.save());

        this.resetBtn = this.root.querySelector('div.control input.reset') as HTMLButtonElement;
        this.resetBtn.addEventListener('click', (ev: MouseEvent) => this.reset());
    }

    private isValid(): boolean {
        if (this.instances.length <= 0)
            return false;
        for (let instance of this.instances)
            if (!instance.isValid())
                return false;
        return true;
    }

    private validate() {
        this.saveBtn.disabled = !this.isValid();
    }

    private makePipelineObj(): any {
        let o: any = {};
        let makeId = (id: number) => `id#${id}`; 
        for (let instance of this.instances) {
            let inputs: {name: string, value: any}[] = [];
            for (let editor of instance.editors) {
                let input: {name: string, value: any};
                switch (editor.type) {
                case 'int':
                    input = {name: editor.name, value: editor.value()};
                    break;
                default:
                    input = {name: editor.name, value: makeId(editor.value())};
                }
                inputs.push(input);
            }
            o[makeId(instance.id)] = {
                block: instance.block.name,
                inputs: inputs,
            }
        }
        return o;
    }

    save() {
        let pipeline = this.makePipelineObj();
        let req = new XMLHttpRequest();
        req.open('POST', '/pipeline');
        req.setRequestHeader('Content-Type', 'application/json');
        let showError = () => 
            alert(`Cannot update pipeline`);
        req.onerror = showError;
        req.onload = () => {
            if (req.status != 200)
                showError();
            else {
                alert('Pipeline updated !');
                this.live.update();
            }
        };
        req.send(JSON.stringify(pipeline));
    }

    private loadPipeline(pipeline: any) {
        let ids = Object.keys(pipeline).sort();
        let idMap: any = {};

        // instantiate blocks
        for (let id of ids) {
            let blockObj = pipeline[id];
            let blockName = blockObj['block'];
            let block = this.blockList.getBlock(blockName);
            let instance = this.instantiate(block);
            idMap[id] = instance;
        }

        // load inputs
        for (let id of ids) {
            let instance: BlockInstance = idMap[id];
            let declInputs = instance.block.inputs;
            let actualInputs = pipeline[id]['inputs']; 
            for (let i=0; i < instance.editors.length; ++i) {
                let editor = instance.editors[i];
                let value = actualInputs[i]['value'];
                let type = declInputs[i].type;
                switch (type) {
                    case 'int':
                        editor.setValue(value);
                        break;
                    default:
                        editor.setValue(idMap[value].id);
                        break;
                }
            }
        }
    };

    private load(): any {
        let req = new XMLHttpRequest();
        req.open('GET', '/pipeline');
        req.onload = () => {
            let pipeline = JSON.parse(req.responseText);
            if (pipeline)
                this.loadPipeline(pipeline);
            this.live.update();
        }
        req.send();
    }

    reset() {
        this.lastId = 0;
        this.instances = [];
        let divs = this.list.querySelectorAll(':scope > div.instance');
        for (let i=0; i < divs.length; ++i) {
            let div = divs.item(i);
            div.remove();
        }
        this.load();
        this.validate();
    }

    private remove(instance: BlockInstance) {
        let instanceEl = instance.root;
        let previousEl = instanceEl.previousElementSibling;
        let nextEl = instanceEl.nextElementSibling;
        this.instances = this.instances.filter((o) => o.id != instance.id);
        instanceEl.remove();
        if (previousEl)
            previousEl.dispatchEvent(new CustomEvent(UPDATE_INSTANCE));
        if (nextEl)
            nextEl.dispatchEvent(new CustomEvent(UPDATE_INSTANCE));
        this.validate();
    }

    private up(instance: BlockInstance) {
        let node = instance.root;
        let previous = node.previousSibling as HTMLElement;
        let parent = node.parentElement;
        parent.insertBefore(node, previous);
        node.dispatchEvent(new CustomEvent(UPDATE_INSTANCE));
        previous.dispatchEvent(new CustomEvent(UPDATE_INSTANCE));
        this.validate();
    }

    private down(instance: BlockInstance) {
        let node = instance.root;
        let next = node.nextSibling as HTMLElement;
        let parent = node.parentElement;
        parent.insertBefore(next, node);
        node.dispatchEvent(new CustomEvent(UPDATE_INSTANCE));
        next.dispatchEvent(new CustomEvent(UPDATE_INSTANCE));
        this.validate();
    }

    private newNumberEditor(name: string): NumberEditor {
        let numberEl = this.numberEditorTmpl.cloneNode(true) as HTMLInputElement;
        numberEl.addEventListener('input', (ev:Event) => this.validate());
        return new NumberEditor(numberEl, name);
    }

    private newOpaqueEditor(name: string, type: string): OpaqueEditor {
        let opaqueEl = this.opaqueInputTmpl.cloneNode(true) as HTMLSelectElement;
        let editor = new OpaqueEditor(opaqueEl, name, type);
        opaqueEl.addEventListener('input', (ev: Event) => this.validate());
        opaqueEl.addEventListener(UPDATE_OPAQUES, (ev: CustomEvent) => {
            let myInstanceEl = opaqueEl.closest('div.instance');
            let myId = parseInt(myInstanceEl.querySelector('td.id').textContent.trim());
            let instancesEl = myInstanceEl.closest('div.instances');
            let allInstances = instancesEl.querySelectorAll(':scope > div.instance');
            let options: number[] = [];
            for (let i=0; i < allInstances.length; ++i) {
                let instance = allInstances.item(i);
                let id = parseInt(instance.querySelector('td.id').textContent.trim());
                if (id == myId) {
                    break;
                }
                let outputType = instance.querySelector('td.output').textContent.trim();
                if (outputType == type) {
                    options.push(id);
                }
            }
            editor.updateOptions(options);
        });
        return editor;
    }

    private newInput(input: Input): [HTMLTableRowElement, Editor] {
        let inputEl = this.inputTmpl.cloneNode(true) as HTMLTableRowElement;
        inputEl.querySelector('td.name').innerHTML = input.name;
        let editor: Editor = null;
        switch (input.type) {
            case 'int':
                editor = this.newNumberEditor(input.name);
                break;
            default:
                editor = this.newOpaqueEditor(input.type, input.type);
                break;
        }
        let editorEl = inputEl.querySelector('td.editor') as HTMLTableDataCellElement;
        editorEl.innerHTML = '';
        editorEl.appendChild(editor.root);
        return [inputEl, editor];
    }

    instantiate(block: Block): BlockInstance {
        let id = this.lastId++;
        let instanceEl = this.instanceTmpl.cloneNode(true) as HTMLDivElement;
        instanceEl.querySelector('td.name').textContent = block.name;
        instanceEl.querySelector('td.id').textContent = `${id}`;

        // inputs
        let tbody = instanceEl.querySelector('tbody.input-list') as HTMLTableSectionElement;
        let editors: Editor[] = [];
        if (block.inputs) {
            for (let input of block.inputs) {
                let [inputEl, editor] = this.newInput(input);
                tbody.appendChild(inputEl);
                editors.push(editor);
            }
        }

        // output
        if (block.output) {
            instanceEl.querySelector('td.output').innerHTML = block.output;
        }

        let instance = new BlockInstance(id, block, editors, instanceEl);
        this.instances.push(instance);

        // remove button
        let removeBtn = instanceEl.querySelector('div.control input.remove') as HTMLButtonElement;
        removeBtn.addEventListener('click', (ev: MouseEvent)=> this.remove(instance));

        // up button
        let upBtn = instanceEl.querySelector('div.control input.up') as HTMLButtonElement;
        upBtn.addEventListener('click', (ev: MouseEvent) => this.up(instance));

        // down button
        let downBtn = instanceEl.querySelector('div.control input.down') as HTMLButtonElement;
        downBtn.addEventListener('click', (ev: MouseEvent) => this.down(instance));
        
        this.list.appendChild(instanceEl);
        let previousInstanceEl = this.list.querySelector(':scope > div.instance:nth-last-child(2)');
        let updateInstance = () => {
            let selects = instanceEl.querySelectorAll('select.opaque');
            for (let i=0; i < selects.length; ++i) {
                let select = selects.item(i);
                select.dispatchEvent(new CustomEvent(UPDATE_OPAQUES));
            }
            upBtn.disabled = (instanceEl.previousSibling == null) 
                || !instanceEl.previousElementSibling.classList.contains('instance');
            downBtn.disabled = (instanceEl.nextSibling == null);
        };
        instanceEl.addEventListener(UPDATE_INSTANCE, (ev: CustomEvent) => updateInstance());
        updateInstance();
        if (previousInstanceEl)
            previousInstanceEl.dispatchEvent(new CustomEvent(UPDATE_INSTANCE));
        this.validate();

        return instance;
    }
}

class Main {
    readonly live: Live;
    readonly blockList: BlockList;
    readonly pipeline: Pipeline;

    constructor(root: HTMLElement) {
        this.blockList = new BlockList(root.querySelector('div.blocks'));
        this.pipeline = new Pipeline(root.querySelector('div.pipeline'));
        this.live = new Live(root.querySelector('div.live'));

        this.blockList.pipeline = this.pipeline;
        this.pipeline.blockList = this.blockList;
        this.pipeline.live = this.live;

        this.blockList.load(() => {
            this.pipeline.reset();
        });
    }
}

window.addEventListener("load", (ev: Event) => new Main(document.body));