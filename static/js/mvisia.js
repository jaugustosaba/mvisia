var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Block = /** @class */ (function () {
    function Block(name, inputs, output) {
        this.name = name;
        this.inputs = inputs;
        this.output = output;
    }
    return Block;
}());
var BlockList = /** @class */ (function () {
    function BlockList(root) {
        this.root = root;
        this.pipeline = null;
        this.blocks = [];
        this.list = root.querySelector('ul.blocks');
        this.blockTempl = this.list.querySelector('div.template li.block');
    }
    BlockList.prototype.getBlock = function (name) {
        for (var _i = 0, _a = this.blocks; _i < _a.length; _i++) {
            var block = _a[_i];
            if (block.name == name)
                return block;
        }
        return null;
    };
    BlockList.prototype.reset = function () {
        var allLIs = this.root.querySelectorAll('div.blocks ul.blocks > li');
        for (var i = 0; i < allLIs.length; ++i) {
            var LI = allLIs.item(i);
            LI.remove();
        }
    };
    BlockList.prototype.load = function (done) {
        var _this = this;
        this.reset();
        var req = new XMLHttpRequest();
        req.open('get', '/blocks');
        req.onload = function () {
            var blocks = JSON.parse(req.responseText);
            for (var name_1 in blocks) {
                var block = blocks[name_1];
                _this.addBlock(name_1, block['inputs'], block['output']);
            }
            if (done)
                done();
        };
        req.onerror = function () {
            alert('Cannot get list of blocks');
        };
        req.send();
    };
    BlockList.prototype.addBlock = function (name, inputs, output) {
        var _this = this;
        var block = new Block(name, inputs, output);
        var blockEl = this.blockTempl.cloneNode(true);
        var nameEl = blockEl.querySelector('p.name');
        nameEl.innerHTML = name;
        var instantiateEl = blockEl.querySelector('input.instantiate');
        instantiateEl.addEventListener('click', function (ev) {
            _this.pipeline.instantiate(block);
        });
        this.list.appendChild(blockEl);
        this.blocks.push(block);
    };
    return BlockList;
}());
var Editor = /** @class */ (function () {
    function Editor(root, name, type) {
        this.root = root;
        this.name = name;
        this.type = type;
    }
    return Editor;
}());
var NumberEditor = /** @class */ (function (_super) {
    __extends(NumberEditor, _super);
    function NumberEditor(root, name) {
        var _this = _super.call(this, root, name, 'int') || this;
        _this.name = name;
        return _this;
    }
    NumberEditor.prototype.hasValue = function () {
        var input = this.root;
        return (input.value != '') && input.willValidate;
    };
    NumberEditor.prototype.value = function () {
        if (!this.hasValue())
            return null;
        return this.root.valueAsNumber;
    };
    NumberEditor.prototype.setValue = function (value) {
        this.root.valueAsNumber = value;
    };
    return NumberEditor;
}(Editor));
var OpaqueEditor = /** @class */ (function (_super) {
    __extends(OpaqueEditor, _super);
    function OpaqueEditor(root, name, type) {
        return _super.call(this, root, name, type) || this;
    }
    OpaqueEditor.prototype.hasValue = function () {
        return this.root.selectedIndex > 0;
    };
    OpaqueEditor.prototype.value = function () {
        if (!this.hasValue())
            return null;
        var el = this.root;
        return parseInt(el.selectedOptions.item(0).value);
    };
    OpaqueEditor.prototype.setValue = function (value) {
        var el = this.root;
        el.selectedIndex = 0;
        for (var i = 1; i < el.options.length; ++i) {
            var option = el.options.item(i);
            if (option.value == value) {
                el.selectedIndex = i;
                break;
            }
        }
    };
    OpaqueEditor.prototype.updateOptions = function (options) {
        var root = this.root;
        // saves old selection
        var curSel;
        if (root.selectedIndex >= 0) {
            curSel = root.options.item(root.selectedIndex).value;
        }
        while (root.hasChildNodes()) {
            root.removeChild(root.firstChild);
        }
        root.appendChild(document.createElement('option'));
        for (var _i = 0, options_1 = options; _i < options_1.length; _i++) {
            var option = options_1[_i];
            var optionEl = document.createElement('option');
            optionEl.value = "" + option;
            optionEl.innerText = optionEl.value;
            optionEl.selected = (curSel == optionEl.value);
            root.appendChild(optionEl);
        }
    };
    return OpaqueEditor;
}(Editor));
var BlockInstance = /** @class */ (function () {
    function BlockInstance(id, block, editors, root) {
        this.id = id;
        this.block = block;
        this.editors = editors;
        this.root = root;
    }
    BlockInstance.prototype.isValid = function () {
        for (var _i = 0, _a = this.editors; _i < _a.length; _i++) {
            var editor = _a[_i];
            if (!editor.hasValue())
                return false;
        }
        return true;
    };
    return BlockInstance;
}());
var Live = /** @class */ (function () {
    function Live(root) {
        this.root = root;
        this.timeoutIds = [];
        this.liveTmpl = root.querySelector('div.template div.live');
    }
    Live.prototype.clearTimeouts = function () {
        for (var _i = 0, _a = this.timeoutIds; _i < _a.length; _i++) {
            var timeoutId = _a[_i];
            clearInterval(timeoutId);
        }
        this.timeoutIds = [];
    };
    Live.prototype.reset = function () {
        this.clearTimeouts();
        var allDivs = this.root.querySelectorAll(':scope > div.live');
        for (var i = 0; i < allDivs.length; ++i)
            allDivs.item(i).remove();
    };
    Live.prototype.fetchLiveCount = function (cb) {
        var req = new XMLHttpRequest();
        req.open('get', '/live');
        req.onload = function () {
            var lives = parseInt(req.responseText.trim());
            cb(lives);
        };
        req.send();
    };
    Live.prototype.appendImage = function (index) {
        var divEl = this.liveTmpl.cloneNode(true);
        this.root.appendChild(divEl);
        divEl.querySelector('h2').innerText = "Stream " + index;
        var imageEl = divEl.querySelector('img');
        var dummy = 0;
        this.timeoutIds.push(setInterval(function () {
            imageEl.src = "/live/" + index + "/" + dummy++;
        }, 250));
    };
    Live.prototype.update = function () {
        var _this = this;
        this.reset();
        this.fetchLiveCount(function (liveCount) {
            for (var i = 0; i < liveCount; ++i) {
                _this.appendImage(i);
            }
        });
    };
    return Live;
}());
var UPDATE_INSTANCE = 'update-instance';
var UPDATE_OPAQUES = 'update-opaques';
var Pipeline = /** @class */ (function () {
    function Pipeline(root) {
        this.root = root;
        this.lastId = 0;
        this.instances = [];
        this.list = root.querySelector('div.instances');
        this.instanceTmpl = this.list.querySelector('div.template div.instance');
        this.numberEditorTmpl = this.list.querySelector('div.template div.number-editor input');
        this.opaqueInputTmpl = this.list.querySelector('div.template div.opaque-editor select');
        this.inputTmpl = this.list.querySelector('div.template div.input tr');
        this.setupCtrls();
    }
    Pipeline.prototype.setupCtrls = function () {
        var _this = this;
        this.saveBtn = this.root.querySelector('div.control input.save');
        this.saveBtn.addEventListener('click', function (ev) { return _this.save(); });
        this.resetBtn = this.root.querySelector('div.control input.reset');
        this.resetBtn.addEventListener('click', function (ev) { return _this.reset(); });
    };
    Pipeline.prototype.isValid = function () {
        if (this.instances.length <= 0)
            return false;
        for (var _i = 0, _a = this.instances; _i < _a.length; _i++) {
            var instance = _a[_i];
            if (!instance.isValid())
                return false;
        }
        return true;
    };
    Pipeline.prototype.validate = function () {
        this.saveBtn.disabled = !this.isValid();
    };
    Pipeline.prototype.makePipelineObj = function () {
        var o = {};
        var makeId = function (id) { return "id#" + id; };
        for (var _i = 0, _a = this.instances; _i < _a.length; _i++) {
            var instance = _a[_i];
            var inputs = [];
            for (var _b = 0, _c = instance.editors; _b < _c.length; _b++) {
                var editor = _c[_b];
                var input = void 0;
                switch (editor.type) {
                    case 'int':
                        input = { name: editor.name, value: editor.value() };
                        break;
                    default:
                        input = { name: editor.name, value: makeId(editor.value()) };
                }
                inputs.push(input);
            }
            o[makeId(instance.id)] = {
                block: instance.block.name,
                inputs: inputs
            };
        }
        return o;
    };
    Pipeline.prototype.save = function () {
        var _this = this;
        var pipeline = this.makePipelineObj();
        var req = new XMLHttpRequest();
        req.open('POST', '/pipeline');
        req.setRequestHeader('Content-Type', 'application/json');
        var showError = function () {
            return alert("Cannot update pipeline");
        };
        req.onerror = showError;
        req.onload = function () {
            if (req.status != 200)
                showError();
            else {
                alert('Pipeline updated !');
                _this.live.update();
            }
        };
        req.send(JSON.stringify(pipeline));
    };
    Pipeline.prototype.loadPipeline = function (pipeline) {
        var ids = Object.keys(pipeline).sort();
        var idMap = {};
        // instantiate blocks
        for (var _i = 0, ids_1 = ids; _i < ids_1.length; _i++) {
            var id = ids_1[_i];
            var blockObj = pipeline[id];
            var blockName = blockObj['block'];
            var block = this.blockList.getBlock(blockName);
            var instance = this.instantiate(block);
            idMap[id] = instance;
        }
        // load inputs
        for (var _a = 0, ids_2 = ids; _a < ids_2.length; _a++) {
            var id = ids_2[_a];
            var instance = idMap[id];
            var declInputs = instance.block.inputs;
            var actualInputs = pipeline[id]['inputs'];
            for (var i = 0; i < instance.editors.length; ++i) {
                var editor = instance.editors[i];
                var value = actualInputs[i]['value'];
                var type = declInputs[i].type;
                switch (type) {
                    case 'int':
                        editor.setValue(value);
                        break;
                    default:
                        editor.setValue(idMap[value].id);
                        break;
                }
            }
        }
    };
    ;
    Pipeline.prototype.load = function () {
        var _this = this;
        var req = new XMLHttpRequest();
        req.open('GET', '/pipeline');
        req.onload = function () {
            var pipeline = JSON.parse(req.responseText);
            if (pipeline)
                _this.loadPipeline(pipeline);
            _this.live.update();
        };
        req.send();
    };
    Pipeline.prototype.reset = function () {
        this.lastId = 0;
        this.instances = [];
        var divs = this.list.querySelectorAll(':scope > div.instance');
        for (var i = 0; i < divs.length; ++i) {
            var div = divs.item(i);
            div.remove();
        }
        this.load();
        this.validate();
    };
    Pipeline.prototype.remove = function (instance) {
        var instanceEl = instance.root;
        var previousEl = instanceEl.previousElementSibling;
        var nextEl = instanceEl.nextElementSibling;
        this.instances = this.instances.filter(function (o) { return o.id != instance.id; });
        instanceEl.remove();
        if (previousEl)
            previousEl.dispatchEvent(new CustomEvent(UPDATE_INSTANCE));
        if (nextEl)
            nextEl.dispatchEvent(new CustomEvent(UPDATE_INSTANCE));
        this.validate();
    };
    Pipeline.prototype.up = function (instance) {
        var node = instance.root;
        var previous = node.previousSibling;
        var parent = node.parentElement;
        parent.insertBefore(node, previous);
        node.dispatchEvent(new CustomEvent(UPDATE_INSTANCE));
        previous.dispatchEvent(new CustomEvent(UPDATE_INSTANCE));
        this.validate();
    };
    Pipeline.prototype.down = function (instance) {
        var node = instance.root;
        var next = node.nextSibling;
        var parent = node.parentElement;
        parent.insertBefore(next, node);
        node.dispatchEvent(new CustomEvent(UPDATE_INSTANCE));
        next.dispatchEvent(new CustomEvent(UPDATE_INSTANCE));
        this.validate();
    };
    Pipeline.prototype.newNumberEditor = function (name) {
        var _this = this;
        var numberEl = this.numberEditorTmpl.cloneNode(true);
        numberEl.addEventListener('input', function (ev) { return _this.validate(); });
        return new NumberEditor(numberEl, name);
    };
    Pipeline.prototype.newOpaqueEditor = function (name, type) {
        var _this = this;
        var opaqueEl = this.opaqueInputTmpl.cloneNode(true);
        var editor = new OpaqueEditor(opaqueEl, name, type);
        opaqueEl.addEventListener('input', function (ev) { return _this.validate(); });
        opaqueEl.addEventListener(UPDATE_OPAQUES, function (ev) {
            var myInstanceEl = opaqueEl.closest('div.instance');
            var myId = parseInt(myInstanceEl.querySelector('td.id').textContent.trim());
            var instancesEl = myInstanceEl.closest('div.instances');
            var allInstances = instancesEl.querySelectorAll(':scope > div.instance');
            var options = [];
            for (var i = 0; i < allInstances.length; ++i) {
                var instance = allInstances.item(i);
                var id = parseInt(instance.querySelector('td.id').textContent.trim());
                if (id == myId) {
                    break;
                }
                var outputType = instance.querySelector('td.output').textContent.trim();
                if (outputType == type) {
                    options.push(id);
                }
            }
            editor.updateOptions(options);
        });
        return editor;
    };
    Pipeline.prototype.newInput = function (input) {
        var inputEl = this.inputTmpl.cloneNode(true);
        inputEl.querySelector('td.name').innerHTML = input.name;
        var editor = null;
        switch (input.type) {
            case 'int':
                editor = this.newNumberEditor(input.name);
                break;
            default:
                editor = this.newOpaqueEditor(input.type, input.type);
                break;
        }
        var editorEl = inputEl.querySelector('td.editor');
        editorEl.innerHTML = '';
        editorEl.appendChild(editor.root);
        return [inputEl, editor];
    };
    Pipeline.prototype.instantiate = function (block) {
        var _this = this;
        var id = this.lastId++;
        var instanceEl = this.instanceTmpl.cloneNode(true);
        instanceEl.querySelector('td.name').textContent = block.name;
        instanceEl.querySelector('td.id').textContent = "" + id;
        // inputs
        var tbody = instanceEl.querySelector('tbody.input-list');
        var editors = [];
        if (block.inputs) {
            for (var _i = 0, _a = block.inputs; _i < _a.length; _i++) {
                var input = _a[_i];
                var _b = this.newInput(input), inputEl = _b[0], editor = _b[1];
                tbody.appendChild(inputEl);
                editors.push(editor);
            }
        }
        // output
        if (block.output) {
            instanceEl.querySelector('td.output').innerHTML = block.output;
        }
        var instance = new BlockInstance(id, block, editors, instanceEl);
        this.instances.push(instance);
        // remove button
        var removeBtn = instanceEl.querySelector('div.control input.remove');
        removeBtn.addEventListener('click', function (ev) { return _this.remove(instance); });
        // up button
        var upBtn = instanceEl.querySelector('div.control input.up');
        upBtn.addEventListener('click', function (ev) { return _this.up(instance); });
        // down button
        var downBtn = instanceEl.querySelector('div.control input.down');
        downBtn.addEventListener('click', function (ev) { return _this.down(instance); });
        this.list.appendChild(instanceEl);
        var previousInstanceEl = this.list.querySelector(':scope > div.instance:nth-last-child(2)');
        var updateInstance = function () {
            var selects = instanceEl.querySelectorAll('select.opaque');
            for (var i = 0; i < selects.length; ++i) {
                var select = selects.item(i);
                select.dispatchEvent(new CustomEvent(UPDATE_OPAQUES));
            }
            upBtn.disabled = (instanceEl.previousSibling == null)
                || !instanceEl.previousElementSibling.classList.contains('instance');
            downBtn.disabled = (instanceEl.nextSibling == null);
        };
        instanceEl.addEventListener(UPDATE_INSTANCE, function (ev) { return updateInstance(); });
        updateInstance();
        if (previousInstanceEl)
            previousInstanceEl.dispatchEvent(new CustomEvent(UPDATE_INSTANCE));
        this.validate();
        return instance;
    };
    return Pipeline;
}());
var Main = /** @class */ (function () {
    function Main(root) {
        var _this = this;
        this.blockList = new BlockList(root.querySelector('div.blocks'));
        this.pipeline = new Pipeline(root.querySelector('div.pipeline'));
        this.live = new Live(root.querySelector('div.live'));
        this.blockList.pipeline = this.pipeline;
        this.pipeline.blockList = this.blockList;
        this.pipeline.live = this.live;
        this.blockList.load(function () {
            _this.pipeline.reset();
        });
    }
    return Main;
}());
window.addEventListener("load", function (ev) { return new Main(document.body); });
